# Compare optimizers
- Against some of these [test functions for optimization](https://en.wikipedia.org/wiki/Test_functions_for_optimization)

# Load data using PySpark
- Run SQL queries on `*.csv`-files

# Build custom Docker image for CI
- Differentiate between local builds and CI for speed