# Optimizer
opt = tf.keras.optimizers.SGD(learning_rate=0.1)

# Minimum
(tf_xmin, tf_ymin) = (2.0, 3.0)

# Starting point for optimization
(tf_x0, tf_y0) = (8.0, 7.0)

# Initialize
tfx = tf.Variable(tf_x0)
tfy = tf.Variable(tf_y0)

# Loss function
tf_loss = lambda: (tfx - tf_xmin)**2 + (tfy - tf_ymin)**2

# Optimize
for idx in range(50):
    opt.minimize(tf_loss, var_list=[tfx, tfy])

print("x: {:.5f}".format(tfx.numpy()))
print("y: {:.5f}".format(tfy.numpy()))
