# Minimum
(torch_xmin, torch_ymin) = (2.0, 3.0)

# Starting point for optimization
(torch_x0, torch_y0) = (8.0, 7.0)

# Initialize
torch_params = torch.tensor([torch_x0, torch_y0], requires_grad=True)

# Optimizer
optimizer = torch.optim.SGD([torch_params], lr=0.2)

# Loss function
def torch_loss(x, y):
    return (x - torch_xmin)**2 + (y - torch_ymin)**2

# Optimize
for idx in range(50):
    optimizer.zero_grad()
    loss = torch_loss(*torch_params)
    loss.backward()
    optimizer.step()

res = torch_params.detach().numpy()

print("x: {:.5f}".format(res[0]))
print("y: {:.5f}".format(res[1]))
