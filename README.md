# Data science notes
[![Build status](https://gitlab.com/bogeholm/datascience-notes/badges/master/pipeline.svg)](https://gitlab.com/bogeholm/datascience-notes/pipelines/)

# Website
[bogeholm.gitlab.io/datascience-notes](https://bogeholm.gitlab.io/datascience-notes/)

# Toolchain
Built with [mdbook](https://github.com/rust-lang/mdBook) and [GitLab CI](https://docs.gitlab.com/ee/ci/yaml/)

# Suggestions for future work
See [projects](projects.md)

# Inspiration
- [github.com/rust-lang-nursery/rust-cookbook](https://github.com/rust-lang-nursery/rust-cookbook)
- [rust-lang-nursery.github.io/rust-cookbook](https://rust-lang-nursery.github.io/rust-cookbook/about.html)
- [raw.githubusercontent.com/rust-lang-nursery/rust-cookbook/master/src/SUMMARY.md](https://raw.githubusercontent.com/rust-lang-nursery/rust-cookbook/master/src/SUMMARY.md)