def f(x):
    return x + 2

x = 5

print(f'f({x}) = {f(x)}')
