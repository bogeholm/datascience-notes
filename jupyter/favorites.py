import matplotlib
import matplotlib.cm as cm
# https://matplotlib.org/3.2.1/tutorials/colors/colormapnorms.html#logarithmic
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os
import shutil
import torch

import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    import tensorflow as tf

from collections import OrderedDict
from IPython.core.pylabtools import figsize
from mpl_toolkits.mplot3d import Axes3D
from torch.autograd import Variable

#matplotlib.rcParams.update({'font.size': 16})
#plt.style.use('seaborn-whitegrid')
#plt.style.use('dark_background')

pwd = os.path.join('.')
stylesheet = os.path.join(pwd, *['mplstyles', 'black-transparent.mplstyle'])
plt.style.use(stylesheet)

# https://matplotlib.org/3.2.1/tutorials/text/usetex.html
required_binaries = ['tex', 'latex', 'gs', 'dvipng']
dependencies_ok = np.array(
    [shutil.which(binary) is not None for binary in required_binaries])

if np.all(dependencies_ok):
    matplotlib.rc('text', usetex=True)
    print('Using LaTeX for formatting text!')
else:
    print('LaTeX is not available')
