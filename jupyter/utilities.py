import matplotlib
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os

def setup3dax(*args, fig=None, projection='3d', ax_kwargs={}, **kwargs):
    """ Setup a 3d axis. Create figure if None. 'args' is passed to fig.add_subplot(args)
        See https://matplotlib.org/3.2.1/api/_as_gen/matplotlib.pyplot.subplot.html
        
        This wrapper merely provides uniform setup, as 3D axes cannot 
        easily be configured using matplotlib styles
    """
    if fig is None:
        fig = plt.figure()
    ax = fig.add_subplot(*args, projection=projection, **ax_kwargs)
    
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False

    ax.xaxis.set_major_locator(ticker.MaxNLocator(5))
    ax.yaxis.set_major_locator(ticker.MaxNLocator(5))
    ax.zaxis.set_major_locator(ticker.MaxNLocator(5))
    
    # Get grid color, set pane to same color
    pane_rgba = colors.to_rgba(plt.rcParams['grid.color'])
    
    ax.w_xaxis.set_pane_color(pane_rgba)
    ax.w_yaxis.set_pane_color(pane_rgba)
    ax.w_zaxis.set_pane_color(pane_rgba)
    
    return fig, ax


def plot3d(X, Y, Z, normalizer=None, **kwargs):
    """ Opinionated 3d plotting
    """
    fig, ax = setup3dax(111)

    # args
    if normalizer is None:
        norm = None
    elif normalizer == 'log':
        norm = colors.LogNorm(vmin=max(Z.min(), 0+np.finfo(np.float64).eps), vmax=Z.max())
        
    ax.plot_surface(X, Y, Z, 
                    norm=norm,
                    cmap=cm.Blues)
    
    ax.set_xlabel('$x$')
    ax.set_ylabel('$y$')
    ax.set_zlabel('$f(x, y)$')
    
    return fig, ax

def figsaver(gfx_dir, name, fig, ax, transparency=True):
    path = os.path.join(gfx_dir, name)
    
    # facecolor='none' will enable transparency
    if transparency:
        facecolor = 'none'
        transparent = True
    else:
        facecolor = fig.get_facecolor()
        transparent = False
    
    fig.savefig(
        path, facecolor=facecolor, edgecolor="none", bbox_inches="tight", dpi=600, transparent=transparent
    )
    print(f"Saved {path}")
