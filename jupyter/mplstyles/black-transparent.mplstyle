# from https://github.com/matplotlib/matplotlib/blob/master/lib/matplotlib/mpl-data/stylelib/seaborn-whitegrid.mplstyle
# Seaborn common parameters
# .15 = dark_gray
# .8 = light_gray
# 4984b8 = 'cool blue' (https://xkcd.com/color/rgb/)

# Reference: https://matplotlib.org/3.2.1/tutorials/introductory/customizing.html
# Anatomy: https://matplotlib.org/3.2.1/gallery/showcase/anatomy.html

text.color: 4984b8
axes.labelcolor: 4984b8
legend.frameon: False
legend.numpoints: 1
legend.scatterpoints: 1
xtick.direction: out
ytick.direction: out
xtick.color: 4984b8
ytick.color: 4984b8
axes.axisbelow: True
image.cmap: Blues

# Fonts
font.family: serif
#font.serif: Times, Palatino, New Century Schoolbook, Bookman, Computer Modern Roman, serif
#font.sans-serif: Arial, Liberation Sans, DejaVu Sans, Bitstream Vera Sans, sans-serif
#font.cursive: Zapf Chancery
#font.monospace: Courier, Computer Modern Typewriter
font.size: 16

# Colors from https://github.com/bogeholm/mikkelsen_mplrc/blob/master/mikkelsen_mplrc/mikkelsen_mplrc.py
# mikl2(), except the dark blue one
axes.prop_cycle: cycler('color', ['xkcd:cerulean', 'xkcd:grass green', 'xkcd:scarlet', 'xkcd:light purple', 'xkcd:orange', 'xkcd:shocking pink', 'xkcd:baby blue', '2ecc71', 'xkcd:lemon yellow'])

grid.linestyle: -
grid.linewidth: 0.5
lines.solid_capstyle: round

lines.color: 4984b8
patch.edgecolor: 4984b8

# Figure color parameters
axes.facecolor: black
figure.facecolor: black
figure.edgecolor: black
savefig.facecolor: black
savefig.edgecolor: black
savefig.transparent: True

# Attempt to remove non-colored lines in 3d plots
figure.frameon: False

# Seaborn whitegrid parameters
axes.grid: True
axes.edgecolor: 4984b8
axes.linewidth: 1
grid.color: 4984b8
xtick.major.size: 0
ytick.major.size: 0
xtick.minor.size: 0
ytick.minor.size: 0
