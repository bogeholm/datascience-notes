import os

from IPython.core.magic import register_cell_magic
from pathlib import Path

@register_cell_magic
def write_and_run(line, cell):
    """ Run cell and write contents to file. 
        Output path and filename, as well as write mode options
        can be specified after the magic invocation.
    """
    # Split input line at whitespace
    cli = line.split()    
    
    # Strip quotes 
    for idx, clix in enumerate(cli):
        cli[idx] = clix.replace("\"", "").replace("\'", "")
    
    # Options start with '-'
    opts = [c for c in cli if c.startswith('-')]
    # Arguments is anything not starting with '-'
    args = [c for c in cli if not c.startswith('-')]

    # Fully qualified path to outfile
    write_path = Path(os.path.join(*args))
    # Directory in which outfile is saved
    write_dir = write_path.parent
    
    # Default mode is 'write' or 'w'
    mode = 'w'
    
    # 'append' or 'a': append mode
    if '-a' in opts or '--append' in opts:
        mode = 'a'
    # 'write' or 'w': override append even though it's default
    if '-w' or '--write' in opts:
        mode = 'w'
    
    # Make directory if not exists, and write
    os.makedirs(write_dir, exist_ok=True)
    with open(write_path, mode) as outfile:
        outfile.write(cell)
    
    # Run cell
    get_ipython().run_cell(cell)