# Articles
## Deep learning 
- [Explainable Deep Learning: A Field Guide for the Uninitiated][article-explainable-deep]
 - [YOLOv4: Optimal Speed and Accuracy of Object Detection][article-yolo-v4]

{{#include ../links.md}}