# Coding

[scikit-learn][software-sklearn]

### Code inclusion example
See [mdBook documentation](https://github.com/rust-lang/mdBook/blob/master/book-example/src/format/mdbook.md)
```python
{{#include ../gists/example.py}}
```

{{#include links.md}}