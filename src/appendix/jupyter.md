# Jupyter
Executing notebook ([stackoverflow](https://stackoverflow.com/questions/35471894/can-i-run-jupyter-notebook-cells-in-commandline)):
```bash
jupyter nbconvert --to notebook --inplace --execute <notebook>.ipynb   
```

Find all notebooks:
```bash
find . -name '*ipynb' | grep -v 'checkpoint'
```

How to run all with `xargs`:
```bash
find . -name '*ipynb' | grep -v 'checkpoint' | xargs jupyter nbconvert --to notebook --inplace --execute
```