<!--
Links, like a *.bib file in LaTeX
-->
<!-- Books -->
[book-dive-into-deep-learning]: http://d2l.ai/

<!-- Articles -->
[article-explainable-deep]: https://arxiv.org/abs/2004.14545
[article-yolo-v4]: https://arxiv.org/abs/2004.10934

<!-- Software -->
[software-keras]: https://keras.io/
[software-pytorch]: https://pytorch.org/
[software-tensorflow]: https://www.tensorflow.org/
[software-sklearn]: https://scikit-learn.org/stable/

<!-- Blog posts -->
