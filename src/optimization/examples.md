# Tensorflow and PyTorch examples

## PyTorch minimal example
```python
{{#include ../../gists/pytorch-optimization-mwe.py}}
```

## Tensorflow minimal example
```python
{{#include ../../gists/tensorflow-optimization-mwe.py}}
```
# Rosenbrock function
## Rosenbrock function - PyTorch
![Rosenbrock function - PyTorch](../gfx/rosenbrock-pytorch.svg)

# Himmelblau's function
## Himmelblau's function - PyTorch
![Himmelblau's function - PyTorch](../gfx/himmelblau-pytorch.svg)

{{#include ../links.md}}