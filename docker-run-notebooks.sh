#! /bin/zsh

docker run -it \
    --volume "$(pwd)":/usr/src/app \
    --rm bogeholm/python-datascience:latest \
    ls \
    #&& echo hello
    find jupyter/. -name '*ipynb' | grep -v 'checkpoints' | xargs jupyter nbconvert --to notebook --inplace --execute
